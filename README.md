Check the tags for the available versions.

Example run

    $ docker run --rm -it --name hugo halberom/hugo:0.15 hugo version
    Hugo Static Site Generator v0.15 BuildDate: 2015-11-25T14:35:21Z

    $ docker run --rm -it -v ~/path/to/your/blog:/root/ --name hugo halberom/hugo hugo server

